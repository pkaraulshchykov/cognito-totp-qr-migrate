import React, { useState } from "react";
import "./App.css";
import {AppContextProvider} from "./AppContext";
import Header from "./components/Header/Header";
import LoginForm from "./components/LoginForm/LoginForm";
import RegistrationForm from "./components/RegistrationForm/RegistrationForm";
import MFA from "./components/MFA/MFA";
import Home from "./components/Home/Home";
import AlertComponent from "./components/AlertComponent/AlertComponent";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  const [title, updateTitle] = useState(null);
  const [errorMessage, updateErrorMessage] = useState(null);


  return (
    <AppContextProvider>
      <Router>
        <div className="App">
          <Header title={title} />
          <div className="container d-flex align-items-center flex-column">
            <Switch>
              <Route path="/" exact={true}>
                <LoginForm
                  showError={updateErrorMessage}
                  updateTitle={updateTitle}
                />
              </Route>
              <Route path="/register">
                <RegistrationForm
                  showError={updateErrorMessage}
                  updateTitle={updateTitle}
                />
              </Route>
              <Route path="/login">
                <LoginForm
                  showError={updateErrorMessage}
                  updateTitle={updateTitle}
                />
              </Route>
              <Route path="/mfa">
                <MFA showError={updateErrorMessage} updateTitle={updateTitle} />
              </Route>
              <Route path="/home">
                <Home />
              </Route>
            </Switch>
            <AlertComponent
              errorMessage={errorMessage}
              hideError={updateErrorMessage}
            />
          </div>
        </div>
      </Router>
    </AppContextProvider>
  );
}

export default App;
