import React, {createContext} from "react";

const AppData = {
    clientId: "7tj95u2eta4evbr13p6k2ak7hn",
    userDetails: {
        userName: "",
        password: ""
    },
    updateUserDetails: () => {},
    mfaSecretCode: "",
    updateMfaSecretCode: () => {},
    authTokens: {
        idToken: "",
        accessToken: "",
        refreshToken: ""
    },
    updateAuthTokens: () => {}
}

const AppContext = createContext(AppData);

class AppContextProvider extends React.Component {

   
    updateUserDetails = (newUserName, newPassword) => {
      this.setState({userDetails: {userName: newUserName, password: newPassword}});
    };

    updateMfaSecretCode = (newMfaSecretCode) => {
        this.setState({mfaSecretCode: newMfaSecretCode});
    };

    updateAuthTokens = (newIdToken, newAccessToken, newRefreshToken) => {
        this.setState({authTokens: {idToken: newIdToken, accessToken: newAccessToken, refreshToken: newRefreshToken}});
    }

    state = {
        userDetails: AppData.userDetails,
        updateUserDetails: this.updateUserDetails,
        mfaSecretCode: AppData.mfaSecretCode,
        updateMfaSecretCode: this.updateMfaSecretCode,
        authTokens: AppData.authTokens,
        updateAuthTokens: this.updateAuthTokens
    };

    render() {
      return (
        <AppContext.Provider value={this.state}>
          {this.props.children}
        </AppContext.Provider>
      );
    }
}

const AppContextConsumer = AppContext.Consumer;

export {AppContext, AppContextProvider, AppContextConsumer};