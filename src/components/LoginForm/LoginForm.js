import React from "react";
import axios from "axios";
import { AppContext } from "../../AppContext";
import { AppContextConsumer } from "../../AppContext";
import "./LoginForm.css";
import { API_BASE_URL } from "../../constants/apiContants";
import { withRouter } from "react-router-dom";

const contextType = AppContext;

class LoginForm extends React.Component {
 
  constructor(props) {
    super(props);
    props.updateTitle("Login to Invisor.ca");
    this.AppData = contextType._currentValue;
    this.state = {
      userName: this.AppData.userDetails.userName,
      password: this.AppData.userDetails.password
    };
  }

  handleChange = (e) => {
    const { id, value } = e.target;
    this.setState((prevState) => ({
      ...prevState,
      [id]: value,
    }));
  };

  handleSubmitClick = () => {
    this.AppData.updateUserDetails(this.state.userName,this.state.password);
    // Call AWS API to Auth
    // Upon return, set Tokens: Access, Id, Refresh
    this.AppData.updateAuthTokens("a","b","c");

    // Call AWS API to check if MFA is set up
    var isMfaSet = false;
    if (isMfaSet) {
      // Do we need an MFA challenge
      // Redirect to "API Calls" apge
    }

    // Call AWS API to get MfaSecretCode

    // Replace with the secret code returned by AWS API
    this.AppData.updateMfaSecretCode("RLNVBIKJ2CEWFPSNJACJV2BGG5FS44YBD4QOZLI7NLKY5Z4ROGMQ");
    this.redirectToMFA();
    return;

    // Original code below
    /*
        e.preventDefault();
        const payload={
            "email":state.userName,
            "password":state.password,
        }

        axios.post(API_BASE_URL+'login', payload)
            .then(function (response) {
                if(response.data.code === 200){
                    setState(prevState => ({
                        ...prevState,
                        'successMessage' : 'Login successful. Redirecting to home page..'
                    }))
                    redirectToHome();
                    props.showError(null)
                }
                else if(response.data.code === 204){
                    props.showError("Username and password do not match");
                }
                else{
                    props.showError("Username does not exists");
                }
            })
            .catch(function (error) {
                props.showError(error.response);
                console.log(error);
            });
        */
  };

  static contextType = AppContext;

  redirectToMFA = () => {
    this.props.history.push("/mfa");
  };

  redirectToHome = () => {
    this.props.updateTitle("Home");
    this.props.history.push("/home");
  };

  redirectToRegister = () => {
    this.props.history.push("/register");
    this.props.updateTitle("Register");
  };

  render() {
    return (
      <div className="card col-12 col-lg-4 login-card mt-2 hv-center">
        <form>
          <div className="form-group text-left">
            <label htmlFor="exampleInputUserName1">User Name</label>
            <input
              type="text"
              className="form-control"
              id="userName"
              aria-describedby="userNameHelp"
              placeholder="Enter User Name"
              value={this.state.userName}
              onChange={this.handleChange}
            />
            <small id="userNameHelp" className="form-text text-muted">
              We'll never share your user name with anyone else.
            </small>
          </div>
          <div className="form-group text-left">
            <label htmlFor="exampleInputPassword1">Password</label>
            <input
              type="password"
              className="form-control"
              id="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-check"></div>
          <button
            type="submit"
            className="btn btn-primary"
            onClick={this.handleSubmitClick}
          >
            Submit
          </button>
        </form>
        <div
          className="alert alert-success mt-2"
          style={{ display: this.state.successMessage ? "block" : "none" }}
          role="alert"
        >
          {this.state.successMessage}
        </div>
        <div className="registerMessage">
          <span>Dont have an account? </span>
          <span
            className="loginText"
            onClick={() => this.redirectToRegister()}
          >
            Register
          </span>
        </div>
      </div>
    );
  }
}

export default withRouter(LoginForm);
