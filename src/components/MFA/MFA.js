import React from "react";
import { AppContext } from "../../AppContext";
import { QRCode } from "react-qr-svg";

class MFA extends React.Component {

  constructor(props) {
    super(props);
    this.props.updateTitle("Set up Multifactor Authentication");
    this.contextType = AppContext;
    this.AppData = this.contextType._currentValue;
    this.state = {
        mfaSecretCode: "otpauth://totp/Cognito Prototype:"+this.AppData.userDetails.userName+"?secret="+this.AppData.mfaSecretCode+"&amp;issuer=Cognito Prototype",
        authenticatorCode: ""
    };
  }

  handleChange = (e) => {
    const { id, value } = e.target;
    this.setState((prevState) => ({
      ...prevState,
      [id]: value,
    }));
  };

  handleSubmitClick() {
      //Upon submit, redirect to "API calls" page
  };

  render() {
    return (
      <div className="mt-2">
        <div>Scan the QR code with your phone's Authenticator application</div>
        <div style={{ textAlign: "center", padding: "50px" }}>
          <QRCode level="H" style={{ width: 256 }} value={this.state.mfaSecretCode} />
        </div>
        <div>Once you had set up an account, enter the code displayed by Authenticator</div>
            <form>
                <div className="form-group text-left">
                    <label htmlFor="exampleAuthenticatorCode">Authenticator code</label>
                    <input
                    type="text"
                    className="form-control"
                    id="authenticatorCode"
                    aria-describedby="authenticatorCodeHelp"
                    placeholder="Enter Authenticator Code"
                    value={this.state.authenticatorCode}
                    onChange={this.handleChange}
                    />
                </div>
                <button
                    type="submit"
                    className="btn btn-primary"
                    onClick={this.handleSubmitClick}
                >
                    Submit
                </button>
            </form>
        </div>
    );
  }
}

export default MFA;
